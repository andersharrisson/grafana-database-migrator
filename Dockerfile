FROM debian:latest

RUN apt-get update && \
    apt-get install -y git sqlite3 gawk && \
    rm -rf /var/lib/apt/lists/* && \
    git clone https://github.com/grafana/database-migrator.git

